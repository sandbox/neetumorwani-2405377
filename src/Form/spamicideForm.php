<?php

/**
 * @file
 * Contains \Drupal\admin_notify\Form\TimeSpentConfigForm.
 */

namespace Drupal\spamicide\Form;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\node\Entity\NodeType;

use Drupal\spamicide\SpamicideManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class spamicideForm extends FormBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spamicide_config_form';
  }


  public function __construct(Connection $connection, SpamicideManager $spamicideManager) {
    $this->connection = $connection;
    $this->spamicideManager = $spamicideManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('spamicide.spamicide_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();
    $form['spamicide_administration_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Spamicide administration links to forms'),
    '#default_value' => \Drupal::config('spamicide.settings')->get('spamicide_admin_mode'),
    '#description' => t("This option will allow enabling/disabling Spamicide on forms. When enabled, users with the '%adminspamicide' permission will see Spamicide administration links on all forms (except on administrative pages, which shouldn't be accessible to untrusted users in the first place). These links make it possible to enable or disable it for the desired type.", array('%adminspamicide' => t('administer spamicide'))),
    );
    $form['spamicide_log_attempts'] = array(
      '#type' => 'checkbox',
      '#title' => t('Log attempts'),
      //'#description' => t('Report information about attempts to the !dblog.', array('!dblog' => l(t('log'), 'admin/reports/dblog'))),
      '#default_value' => \Drupal::config('spamicide.settings')->get('spamicide_log_attempts'),
    );
    $form['spamicide_description'] = array(
      '#type' => 'textfield',
      '#size' => 90,
      '#title' => t('Spamicide Description Message'),
      '#description' => t("Set the spamicide description message to further hide it's signature."),
      '#default_value' => \Drupal::config('spamicide.settings')->get('spamicide_description'),
      '#required' => TRUE,
    );
    $form['spamicide_forms'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Spamicide to a form or remove an added form'),
      '#description' => t("Select from the listed forms (identified by their <strong>form_id</strong>'s). You can easily add arbitrary forms with the help of the <strong>'Add Spamicide administration links to forms'</strong> option above then navigating to any form."),
    );

    $form['spamicide_forms']['spamicide_form'] = array(
      '#tree' => TRUE,
      '#theme' => 'spamicide_admin_settings',
    );

    // list all possible form_id's
    $query = $this->connection->select('spamicide', 's');
    $query->fields('s');
    $query->orderBy('s.form_id');
    $result = $query->execute();
    while ($spamicide = $result->fetchAssoc()) {
      $form['spamicide_forms']['spamicide_form'][$spamicide['form_id']]['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => $spamicide['enabled'],
      );
      $form['spamicide_forms']['spamicide_form'][$spamicide['form_id']]['form_id'] = array(
        '#markup' => ($spamicide['form_id']),
      );
      $form['spamicide_forms']['spamicide_form'][$spamicide['form_id']]['form_field'] = array(
        '#type' => 'textfield',
        '#size' => 30,
        '#default_value' => $spamicide['form_field'],
      );
      // additional operations
      $url = Url::fromRoute('spamicide.spamicide_form_delete' , ['form_id' => $spamicide['form_id']] );
      $internal_link = \Drupal::l(t('Delete'), $url);
      if ($spamicide['removable']) {
        $form['spamicide_forms']['spamicide_form'][$spamicide['form_id']]['operations'] = array(
          '#markup' => $internal_link,
        );
      }
      else {
        $form['spamicide_forms']['spamicide_form'][$spamicide['form_id']]['operations'] = array(
          '#markup' => "N/A",
        );
      }
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    foreach ($input['spamicide_form'] as $spamicide_form_id => $data) {
      if ($data['enabled']) {
        if (!$data['form_field']) {
          $form_state->setErrorByName('spamicide_form][' . $spamicide_form_id . '][form_field', $this->t("%field field name cannot be empty", array('%field' => $spamicide_form_id)));
        }
        if (preg_match_all('[\W]', $data['form_field'], $str)) {
          $form_state->setErrorByName('spamicide_form][' . $spamicide_form_id . '][form_field', $this->t("Only AlphaNumeric characters or the underscore please"));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $userInputValues = $form_state->getUserInput();
    $config = \Drupal::configFactory()->getEditable('spamicide.settings');
    $config->set('spamicide_admin_mode', $userInputValues['spamicide_administration_mode']);
    $config->set('spamicide_log_attempts' , $userInputValues['spamicide_log_attempts']);
    $config->set('spamicide_description' , $userInputValues['spamicide_description']);
    $config->save();
    foreach ($userInputValues['spamicide_form'] as $spamicide_form_id => $data) {
      $this->connection->merge('spamicide')
        ->key('form_id', $spamicide_form_id)
        ->fields(array('enabled' => $data['enabled'], 'form_field' => $data['form_field']))
        ->execute();
    }
  }
}
