<?php

/**
 * @file
 * Contains \Drupal\admin_notify\Form\TimeSpentConfigForm.
 */

namespace Drupal\spamicide\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class spamicideDeleteForm extends ConfirmFormBase {
  public function getFormId() {
    return 'spamicide_delete_config_form';
  }
   public function getQuestion() {
    return t('Do you want to delete %id?', array('%id' => $this->id));
  }
     public function getCancelUrl() {
        return new Url('spamicide.spamicide_config');
    }
   public function getDescription() {
    return t('Only do this if you are sure!');
  }
  public function getConfirmText() {
    return t('Delete it!');
  }
  public function getCancelText() {
    return t('Nevermind');
  }
  public function buildForm(array $form, FormStateInterface $form_state, $form_id='') {
    $this->form_id = $form_id;

    return parent::buildForm($form, $form_state);
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->spamicide_delete($this->form_id);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
  public function spamicide_delete($form_id) {
     $spamicide_form_id = $form_id;
     $spamicide_form_field = db_query('SELECT form_field FROM {spamicide} WHERE form_id = :form_id', array(':form_id' => $spamicide_form_id))->fetchField();
     db_delete('spamicide')->condition('form_id', $form_id)->execute();
    // _spamicide_set_css_file($spamicide_form_field, 'delete');
     drupal_set_message(t('Spamicide protection for form %form_id has been removed.', array('%form_id' => $spamicide_form_id)));
     return;
  }
}

