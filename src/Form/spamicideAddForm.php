<?php

/**
 * @file
 * Contains \Drupal\admin_notify\Form\TimeSpentConfigForm.
 */

namespace Drupal\spamicide\Form;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\spamicide\SpamicideManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class spamicideAddForm extends FormBase implements ContainerInjectionInterface {
  public function getFormId() {
    return 'spamicide_add_config_form';
  }

  public function __construct(Connection $connection, SpamicideManager $spamicideManager) {
    $this->connection = $connection;
    $this->spamicideManager = $spamicideManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('spamicide.spamicide_manager')
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $form = array();
    $form_id_from_url = ($request->get('form_id'));
    $form['spamicide_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form ID'),
    '#description' => t('The Drupal form_id of the form to add the Spamicide to.'),
    '#value' => $form_id_from_url,
    '#disabled' => TRUE,
  );
  // will have to set default for this is a form/variable
  $form['spamicide_form_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Form field'),
    '#default_value' => 'feed_me',
    '#description' => t('The name you want for the field. Use only letters, numbers, and the underscore character(_).'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = ($form_state->getValues());
    if (preg_match_all('[\W]', $form_values['spamicide_form_field'], $str)) {
      $form_state->setErrorByName('spamicide_form_field', $this->t('Only AlphaNumeric characters or the underscore please'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state_values = $form_state->getValues();
    // remove old settings
    $this->connection->delete('spamicide')->condition('form_id', $form_state_values['spamicide_form_id'])->execute();

    // save new settings
    $this->connection->insert('spamicide')
      ->fields(array(
        'form_id' => $form_state_values['spamicide_form_id'],
        'form_field' => $form_state_values['spamicide_form_field'],
        'enabled' => 1,
      ))
      ->execute();

    $this->spamicideManager->setCssFile($form_state_values['spamicide_form_field'], 'create');
    drupal_set_message(t('Saved Spamicide settings.'));
    // redirect to general Spamicide settings page after submission
    return new RedirectResponse(\Drupal::url('spamicide.spamicide_config'));
  }
}
