<?php

/**
 * @file
 * Definition of Drupal\spamicide\SpamicideManager
 */

namespace Drupal\spamicide;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Render\Element;

class SpamicideManager implements SpamicideManagerInterface{

  /**
   * Constructs a SpamicideManager Object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   */
  public function __construct(Connection $connection, ConfigFactory $configFactory) {
    $this->connection = $connection;
    $this->config = $configFactory->get('spamicide.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getField($form) {
    $query = $this->connection->select('spamicide', 's');
    $query->fields('s', array('enabled', 'form_field'));
    $query->condition('form_id', $form);

    $result = $query->execute()->fetchAssoc();

    if (is_array($result) && $result['enabled']) {
      return $result['form_field'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCssFile($form_field, $op) {
    $spam_path = 'public://' . $this->config->get('spamicide_dir');
    $css_file = $spam_path . '/' . $form_field . '.css';
    if ($op == 'delete') {
      $query = $this->connection->select('spamicide', 's');
      $query->fields('s', 'form_field');
      $query->condition('form_field', $form_field);
      $query->distinct();

      $file_check = $query->execute()->fetchCol();

      if (!$file_check || !in_array($form_field , $file_check)) {
        file_unmanaged_delete($css_file);
      }
    }
    if ($op == 'create' && !file_exists($css_file)) {
      $className = 'edit-' . str_replace('_', '-', $form_field) . '-wrapper';
      $css = 'div.' . $className . ' { display:none; }';
      file_save_data($css, $css_file, FILE_EXISTS_REPLACE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription($lang_code = NULL) {
    $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $default = t('To prevent automated spam submissions leave this field empty.');
    if (\Drupal::moduleHandler()->moduleExists('locale')) {
      $description = $this->config->get("spamicide_description_$lang_code", $default);
    }
    else {
      $description = $this->config->get('spamicide_description', $default);
    }
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldPostRender($content, $element) {
    return '<div class="' . $this->getCssClass($element['#name']) . '">' . $content . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function getCssClass($name) {
    return 'edit-' . str_replace('_', '-', $name) . '-wrapper';
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldTitle($form_field) {
    return str_replace('_', ' ', $form_field);
  }

  /**
   * {@inheritdoc}
   */
  function fieldPreRender(&$form, $form_id) {
    // search the weights of the buttons in the form
    $button_weights = array();
    foreach ((Element::children($form)) as $key) {
      if ($key == 'buttons' || isset($form[$key]['#type']) && ($form[$key]['#type'] == 'submit')) {
        $button_weights[] = $form[$key]['#weight'];
      }
    }
    if ($button_weights) {
      // set the weight of the Spamicide element a tiny bit smaller than the lightest button weight
      // (note that the default resolution of #weight values is 1/1000 (see drupal/includes/form.inc))
      $first_button_weight = min($button_weights);
      $spamicide_field = \Drupal::service('spamicide.spamicide_manager')->getField($form_id);
      if ($spamicide_field) {
        $form[$spamicide_field]['#weight'] = $first_button_weight - 0.5/1000.0;
      }
      else {
        $form['spamicide']['#weight'] = $first_button_weight - 0.5/1000.0;
      }
      // make sure the form gets sorted before rendering
      unset($form['#sorted']);
    }
    return $form;
  }
}