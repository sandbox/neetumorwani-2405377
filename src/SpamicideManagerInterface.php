<?php

/**
 * @file
 * Definition of Drupal\spamicide\SpamicideManagerInterface.
 */

namespace Drupal\spamicide;

interface SpamicideManagerInterface {
  /**
   * Get the spamicide field name and .css file_name
   * @param $form
   * @return the spamicide field name and .css file_name to call or FALSE
   * @todo re-work this to prevent errors showing up
   */
  public function getField($form);

  /**
   * Set the spamicide field name and .css file_name
   * @param array $form_field
   * @param string $op
   */
  public function setCssFile($form_field, $op);

  /**
   * Show translation if available
   * @param $lang_code
   * @return translated field description
   * @todo change $default to be able to change it in the admin interface
   * 	 with the possibility of adding per form default description
   */
  public function getDescription($lang_code=NULL);

  /**
   * #post_render callback on the spamicide field. This will wrap the field
   * in a div so it can be hidden with CSS.
   *
   */
  public function fieldPostRender($content, $element);

  /**
   * Place the spamicide field just before the submit button
   * @param $form_id
   * @param $form
   * @todo figure out how to make this work
   */
  function fieldPreRender(&$form, $form_id);

  /**
   * Returns a CSS class name based on the field's name. Should be used
   * to wrap around the field so it can be hidden.
   *
   * Note that it should be pretty generic so it can't be sniffed out
   * by a spammer.
   *
   * @param string $name
   * 		The field's name
   * @return string
   * 		The CSS class
   */
  public function getCssClass($name);

  /**
   * Returns a Human readable form field title by replacing '-'  with spaces
   *
   * @param $form_field
   *    Form field's name
   * @return mixed
   */
  public function setFieldTitle($form_field) ;
}