<?php

use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\String;

/**
 * Implements hook_help().
 */
function spamicide_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.spamicide':
      $output = '';
      $output = 'Spamicide';
      $output .= '<p>' . t("Spamicide is intended to prevent spam without user interaction. Select the forms you'd like to attach spamicide to and a form field will be added but hidden by css. If the field is filled by anything (including humans that think they know better) it will prevent submission into you site.") . '</p>';
      $output .= '<p>' . t('To add Spamicide to a form once you\'ve enabled adding admin links, navigate to that form, and click the link "Add Spamicide protection to this form"') . '</p>';
       return $output;
  }
}

/**
 * Implements hook_theme()
 */
function spamicide_theme() {
  return array(
    'spamicide_admin_settings' => array(
      'render element' => 'form',
      'function' => 'theme_spamicide_admin_settings'
    ),
  );
}

/**
 * Theme callback for 'spamicide_admin_settings'
 */
function theme_spamicide_admin_settings($variables) {
  $form = $variables['form'];
  $header = array(t('Enabled'), t('Form_id'), t('Form field name'), t('Delete'));
  $rows = array();
  $form_elements = Element::children($form);
  foreach ($form_elements as $key) {
    $row = array();
    $row[] = render($form[$key]['enabled']);
    $row[] = render($form[$key]['form_id']);
    $row[] = render($form[$key]['form_field']);
    $row[] = render($form[$key]['operations']);
    $rows[] = $row;
  }

  $table_element = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );

  return render($table_element);
}

/**
 * Implements hook_form_alter().
 */
function spamicide_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $account = \Drupal::currentUser();
  $spamicide_manager = \Drupal::service('spamicide.spamicide_manager');
  $spamicide_config = \Drupal::config('spamicide.settings');
  $current_route_string = Url::fromRoute('<current>')->toString();
  $path_args = explode('/', $current_route_string);
  $new_form_id = $form_id;
  if(isset($path_args['3'])) {
    if($path_args['3'] == 'edit') {
    $new_form_id = (str_replace("edit_","",$form_id));
    }
  }

  $spamicide_field = $spamicide_manager->getField($new_form_id);
  if ($spamicide_field) {
    $spamicide_description = $spamicide_manager->getDescription();
    $spamicide_destination = drupal_get_destination();

    $form[$spamicide_field] = [
      '#title' => $spamicide_manager->setFieldTitle($spamicide_field),
      '#type' => 'textfield',
      '#size' => 20,
      //'#description' => String::checkPlain($spamicide_description),
      '#pre_render' => array_merge(array('_spamicide_pre_render'), element_info_property('textfield', '#pre_render', array())),
      '#name' => $spamicide_field,
      '#post_render' => array_merge(array('_spamicide_post_render'), element_info_property('textfield', '#post_render', array())),
    ];

    $className = '.edit-' . str_replace('_', '-', $spamicide_field) . '-wrapper';
    $form[$spamicide_field]['#attached']['html_head'][] = [[
      '#tag' => 'style',
      '#value' => $className . ' { display: none !important; }',
    ], 'spamicide_css'];

    $form['destination'] = array(
      '#type' => 'value',
      '#value' => $spamicide_destination['destination'],
    );
    $form['spamicide']['#element_validate'] = array('spamicide_validate');
  }
  elseif ($account->hasPermission('administer spamicide') && $spamicide_config->get('spamicide_admin_mode') && ($path_args[1] != 'admin')) {
    $form['spamicide'] = array(
      '#type' => 'fieldset',
      '#title' => t('Spamicide'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['spamicide']['add_spamicide'] = array(
      '#markup' => \Drupal::l(t('Add Spamicide protection to this form.'), Url::fromRoute('spamicide.spamicide_form_add', array('form_id' => $new_form_id))),
    );
  }
  else {
    return;
  }
}

/**
 * Custom validate callback by spamicide
 * Implements hook_validate().
 * @param $form_values
 * @return none
 */
function spamicide_validate(array $form, FormStateInterface &$form_state) {
  $spamicide_manager = \Drupal::service('spamicide.spamicide_manager');
  $editable_spamicide_config = \Drupal::configFactory()->getEditable('spamicide.settings');;
  $input = $form_state->getUserInput();
  $spamicide_field = $spamicide_manager->getField($input['form_id']);
  if (!$spamicide_field) {
    return;
  }
  elseif (empty($input[$spamicide_field])) {
    return;
  }
  else {
    // update attempt counter
    $editable_spamicide_config->set('spamicide_attempt_counter', $editable_spamicide_config->get('spamicide_attempt_counter') + 1);
    // log to watchdog if needed
    if ($editable_spamicide_config->get('spamicide_log_attempts')) {

      \Drupal::logger('spamicide')->notice(
        t('%form_id post blocked by Spamicide module: their IP address is "%ipaddress".',
          array('%form_id' => $input['form_id'], '%ipaddress' => Drupal::request()->getClientIp()))
      );
    }
  }
}

function _spamicide_pre_render(&$form) {
  // search the weights of the buttons in the form
  $button_weights = array();
  foreach ((Element::children($form)) as $key) {
    if ($key == 'buttons' || isset($form[$key]['#type']) && ($form[$key]['#type'] == 'submit')) {
      $button_weights[] = $form[$key]['#weight'];
    }
  }
  if ($button_weights) {
    // set the weight of the Spamicide element a tiny bit smaller than the lightest button weight
    // (note that the default resolution of #weight values is 1/1000 (see drupal/includes/form.inc))
    $first_button_weight = min($button_weights);
    $spamicide_field = \Drupal::service('spamicide.spamicide_manager')->getField($form_id);
    if ($spamicide_field) {
      $form[$spamicide_field]['#weight'] = $first_button_weight - 0.5/1000.0;
    }
    else {
      $form['spamicide']['#weight'] = $first_button_weight - 0.5/1000.0;
    }
    // make sure the form gets sorted before rendering
    unset($form['#sorted']);
  }
  return $form;
}

function _spamicide_post_render($content, $element) {
  return '<div class="' . \Drupal::service('spamicide.spamicide_manager')->getCssClass($element['#name']) . '">' . $content . '</div>';
}
